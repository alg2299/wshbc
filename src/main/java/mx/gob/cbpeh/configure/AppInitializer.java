package mx.gob.cbpeh.configure;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	 @Override
	    protected Class < ? > [] getRootConfigClasses() {
	        return new Class[] {
	           JPAConfig.class
	        };
	        //return null;
	    }

	    @Override
	    protected Class < ? > [] getServletConfigClasses() {
	        return new Class[] {
	            WsConfig.class
	        };
	    }

	    @Override
	    protected String[] getServletMappings() {
	        return new String[] {
	            "/"
	        };
	    }
}
