package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTipoComunicado;


public interface CatTipoComunicadoServicio {

    public List < CatTipoComunicado > getCatTipoComunicados();

    public void saveCatTipoComunicado(CatTipoComunicado tipoComunicado);

    public Optional<CatTipoComunicado>  getCatTipoComunicado(int idTipoComunicado) throws ResourceNotFoundException;

    public void deleteCatTipoComunicado(int idTipoComunicado) throws ResourceNotFoundException;
}
