package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstado;


public interface CatEstadoServicio {

    public List < CatEstado > getCatEstados();
    
    public CatEstado getEstadoCodigoEstado(String codigoEstado);

    public void saveCatEstado(CatEstado estado);

    public Optional<CatEstado>  getCatEstado(int idEstado) throws ResourceNotFoundException;

    public void deleteCatEstado(int idEstado) throws ResourceNotFoundException;
}
