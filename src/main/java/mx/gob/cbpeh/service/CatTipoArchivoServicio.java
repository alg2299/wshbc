package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTipoArchivo;


public interface CatTipoArchivoServicio {

    public List < CatTipoArchivo > getCatTipoArchivos();

    public void saveCatTipoArchivo(CatTipoArchivo tipoArchivo);

    public Optional<CatTipoArchivo>  getCatTipoArchivo(int idTipoArchivo) throws ResourceNotFoundException;
    
    public CatTipoArchivo  getCatTipoArchivo(String extension) throws ResourceNotFoundException;

    public void deleteCatTipoArchivo(int idTipoArchivo) throws ResourceNotFoundException;
}
