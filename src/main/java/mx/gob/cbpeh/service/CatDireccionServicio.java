package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatDireccion;
import mx.gob.cbpeh.model.CatEstatus;


public interface CatDireccionServicio {

    public List < CatDireccion > getCatDirecciones();
    
    public List < CatDireccion > getCatDireccionesActivas(CatEstatus catEstatus);

    public void saveCatDireccion(CatDireccion direccion);

    public Optional<CatDireccion>  getCatDireccion(int idDireccion) throws ResourceNotFoundException;

    public void deleteCatDireccion(int idDireccion) throws ResourceNotFoundException;
}
