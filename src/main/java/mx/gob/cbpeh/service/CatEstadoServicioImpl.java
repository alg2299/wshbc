package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstado;
import mx.gob.cbpeh.repository.CatEstadoRepositorio;

@Service
public class CatEstadoServicioImpl implements CatEstadoServicio {
	
	@Autowired
	private CatEstadoRepositorio estadoRepositorio;

	@Override
	@Transactional
	public List<CatEstado> getCatEstados() {
		return estadoRepositorio.findAll();
	}
	
	@Override
	@Transactional
	 public CatEstado getEstadoCodigoEstado(String codigoEstado){
		return estadoRepositorio.findByCodigoEstado(codigoEstado);
	}
	
	@Override
	@Transactional
	public void saveCatEstado(CatEstado estado) {
		estadoRepositorio.save(estado);		
	}

	@Override
	@Transactional
	public Optional<CatEstado> getCatEstado(int idEstado) throws ResourceNotFoundException {
		return estadoRepositorio.findById(idEstado);
	}

	@Override
	@Transactional
	public void deleteCatEstado(int idEstado) throws ResourceNotFoundException {
		estadoRepositorio.deleteById(idEstado);		
	}

}
