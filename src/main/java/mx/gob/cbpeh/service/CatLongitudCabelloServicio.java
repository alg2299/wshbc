package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatLongitudCabello;


public interface CatLongitudCabelloServicio {

    public List < CatLongitudCabello > getCatLongitudCabello();

    public void saveCatLongitudCabello(CatLongitudCabello longitudCabello);

    public Optional<CatLongitudCabello>  getCatLongitudCabello(int idCatLongitudCabello) throws ResourceNotFoundException;

    public void deleteCatLongitudCabello(int idCatLongitudCabello) throws ResourceNotFoundException;
}
