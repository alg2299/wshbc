package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatMedioContacto;
import mx.gob.cbpeh.repository.CatMedioContactoRepositorio;

@Service
public class CatMedioContactoServicioImpl implements CatMedioContactoServicio {
	
	@Autowired
	private CatMedioContactoRepositorio medioContactoRepositorio;

	@Override
	@Transactional
	public List<CatMedioContacto> getCatMedioContactos() {
		return medioContactoRepositorio.findAll();
	}

	@Override
	@Transactional
	public void saveCatMedioContacto(CatMedioContacto medioContacto) {
		medioContactoRepositorio.save(medioContacto);
	}

	@Override
	@Transactional
	public Optional<CatMedioContacto> getCatMedioContacto(int idMedioContacto) throws ResourceNotFoundException {
		return medioContactoRepositorio.findById(idMedioContacto);
	}

	@Override
	@Transactional
	public void deleteCatMedioContacto(int idMedioContacto) throws ResourceNotFoundException {
		medioContactoRepositorio.deleteById(idMedioContacto);
	}

}
