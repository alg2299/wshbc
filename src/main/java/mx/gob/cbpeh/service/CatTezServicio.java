package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTez;


public interface CatTezServicio {

    public List < CatTez > getCatTez();

    public void saveCatTez(CatTez tez);

    public Optional<CatTez>  getCatTez(int idTez) throws ResourceNotFoundException;

    public void deleteCatTez(int idTez) throws ResourceNotFoundException;
}
