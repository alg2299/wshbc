package mx.gob.cbpeh.service;

import java.util.List;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.Usuario;


public interface UsuarioServicio {

    public List < Usuario > getUsuarios();

    public void saveUsuario(Usuario Usuario);

    public Usuario getUsuario(String idUsuario) throws ResourceNotFoundException;
    
    public Usuario getUsuarioEstatus(String idUsuario, CatEstatus catEstatus) throws ResourceNotFoundException;

    public void deleteUsuario(int idUsuario) throws ResourceNotFoundException;
}
