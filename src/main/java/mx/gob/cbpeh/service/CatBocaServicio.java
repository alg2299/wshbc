package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatBoca;


public interface CatBocaServicio {

    public List < CatBoca > getCatBocas();

    public void saveCatBoca(CatBoca boca);

    public Optional<CatBoca>  getCatBoca(int idBoca) throws ResourceNotFoundException;

    public void deleteCatBoca(int idBoca) throws ResourceNotFoundException;
}
