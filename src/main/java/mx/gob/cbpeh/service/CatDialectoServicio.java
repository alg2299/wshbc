package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatDialecto;


public interface CatDialectoServicio {

    public List < CatDialecto > getCatDialectos();

    public void saveCatDialecto(CatDialecto dialecto);

    public Optional<CatDialecto>  getCatDialecto(int idDialecto) throws ResourceNotFoundException;

    public void deleteCatDialecto(int idDialecto) throws ResourceNotFoundException;
}
