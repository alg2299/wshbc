package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatMunicipio;
import mx.gob.cbpeh.repository.CatMunicipioRepositorio;

@Service
public class CatMunicipioServicioImpl implements CatMunicipioServicio {
	
	@Autowired
	private CatMunicipioRepositorio municipioRepositorio;

	@Override
	@Transactional
	public List<CatMunicipio> getCatMunicipios() {
		return municipioRepositorio.findAll();
	}
	
	@Override
	@Transactional
	public List<CatMunicipio> getCatMunicipiosPorEstado(String codigoEstado) {
	
		return municipioRepositorio.findByCodigoEstado(codigoEstado);
	}

	
	@Override
	@Transactional
	public CatMunicipio getCatMunicipiosPorEstadoPorMunicipio(String codigoEstado, String codigoMunicipio) {
	
		return municipioRepositorio.findByCodigoEstadoAndCodigoMunicipio(codigoEstado, codigoMunicipio);
	}
	
	@Override
	@Transactional
	public void saveCatMunicipio(CatMunicipio municipio) {
		municipioRepositorio.save(municipio);

	}

	@Override
	@Transactional
	public Optional<CatMunicipio> getCatMunicipio(int idMunicipio) throws ResourceNotFoundException {
		return municipioRepositorio.findById(idMunicipio);
	}

	@Override
	@Transactional
	public void deleteCatMunicipio(int idMunicipio) throws ResourceNotFoundException {
		municipioRepositorio.deleteById(idMunicipio);
	}
	
}
