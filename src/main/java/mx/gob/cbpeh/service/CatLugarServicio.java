package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatLugar;


public interface CatLugarServicio {

    public List < CatLugar > getCatLugar();

    public void saveCatLugar(CatLugar lugar);

    public Optional<CatLugar>  getCatLugar(int idLugar) throws ResourceNotFoundException;

    public void deleteCatLugar(int idLugar) throws ResourceNotFoundException;
}
