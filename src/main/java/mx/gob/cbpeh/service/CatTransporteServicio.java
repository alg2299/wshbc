package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTransporte;


public interface CatTransporteServicio {

    public List < CatTransporte > getCatTransportes();

    public void saveCatTransporte(CatTransporte transporte);

    public Optional<CatTransporte>  getCatTransporte(int idTransporte) throws ResourceNotFoundException;

    public void deleteCatTransporte(int idTransporte) throws ResourceNotFoundException;
}
