package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.PersonaIncompetencia;


public interface PersonaIncompetenciaServicio {

    public List < PersonaIncompetencia > getPersonaIncompetencias();

    public void savePersonaIncompetencia(PersonaIncompetencia personaIncompetencia);

    public Optional<PersonaIncompetencia>  getPersonaIncompetencia(int idPersonaIncompetencia) throws ResourceNotFoundException;

    public void deletePersonaIncompetencia(int IdPersonaIncompetencia) throws ResourceNotFoundException;
}
