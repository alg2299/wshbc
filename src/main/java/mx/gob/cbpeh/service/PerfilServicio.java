package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.Perfil;


public interface PerfilServicio {

    public List < Perfil > getPerfiles();
    
    public List < Perfil > getPerfilesActivos(CatEstatus catEstatus);

    public void savePerfil(Perfil perfil);

    public Optional<Perfil>  getPerfil(int idPerfil) throws ResourceNotFoundException;

    public void deletePerfil(int idPerfil) throws ResourceNotFoundException;
}
