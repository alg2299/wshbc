package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEdad;


public interface CatEdadServicio {

    public List < CatEdad > getCatEdades();

    public void saveCatEdad(CatEdad edad);

    public Optional<CatEdad>  getCatEdad(int idEdad) throws ResourceNotFoundException;

    public void deleteCatEdad(int idEdad) throws ResourceNotFoundException;
}
