package mx.gob.cbpeh.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatCatalogosDB;
import mx.gob.cbpeh.repository.CatCatalogosDBRepositorio;


@Service
public class CatCatalogosDBServicioImpl implements CatCatalogosDBServicio {
	
	@Autowired
	private CatCatalogosDBRepositorio catalogosDBRepositorio;

	@Override
	@Transactional
	public List<CatCatalogosDB> getCatCatalogos() {
		return catalogosDBRepositorio.findAll();
	}

	@Override
	@Transactional
	public void saveCatCatalogo(CatCatalogosDB complexion) {
		catalogosDBRepositorio.save(complexion);		
	}

	@Override
	@Transactional
	public CatCatalogosDB getCatCatalogo(int idComplexion) throws ResourceNotFoundException {
		return catalogosDBRepositorio.findByIdCatalogo(idComplexion);
	}

	@Override
	@Transactional
	public void deleteCatCatalogo(int idComplexion) throws ResourceNotFoundException {
		catalogosDBRepositorio.deleteById(idComplexion);		
	}
}
