package mx.gob.cbpeh.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.Usuario;
import mx.gob.cbpeh.repository.UsuarioRepositorio;

@Service
public class UsuarioServicioImpl implements UsuarioServicio {
	
	@Autowired
	private UsuarioRepositorio usuarioRepositorio;
	
	@Override
	@Transactional
	public List<Usuario> getUsuarios() {
		return usuarioRepositorio.findAll();
	}

	@Override
	@Transactional
	public void saveUsuario(Usuario usuario) {
		usuarioRepositorio.save(usuario);
	}

	@Override
	@Transactional
	public Usuario getUsuario(String idUsuario) throws ResourceNotFoundException {
		return usuarioRepositorio.findByIdUsuario(idUsuario); 
	}
	
	@Override
	@Transactional
	public Usuario getUsuarioEstatus(String idUsuario, CatEstatus catEstus) throws ResourceNotFoundException {
		return usuarioRepositorio.findByIdUsuarioAndCatEstatus(idUsuario,catEstus); 
	}

	@Override
	@Transactional
	public void deleteUsuario(int idUsuario) throws ResourceNotFoundException {
		usuarioRepositorio.deleteById(idUsuario);
	}

}
