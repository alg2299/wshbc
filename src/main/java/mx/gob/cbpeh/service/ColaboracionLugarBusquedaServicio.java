package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.ColaboracionLugarBusqueda;


public interface ColaboracionLugarBusquedaServicio {

    public List < ColaboracionLugarBusqueda > getColaboracionLugarBusqueda();

    public ColaboracionLugarBusqueda saveColaboracionLugarBusqueda(ColaboracionLugarBusqueda catLugarBusqueda);

    public Optional<ColaboracionLugarBusqueda>  getColaboracionLugarBusqueda(int idColaboracionLugarBusqueda) throws ResourceNotFoundException;

    public void deleteColaboracionLugarBusqueda(int idColaboracionLugarBusqueda) throws ResourceNotFoundException;
}
