package mx.gob.cbpeh.service;

import java.util.List;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatCatalogosDB;


public interface CatCatalogosDBServicio {

    public List < CatCatalogosDB > getCatCatalogos();

    public void saveCatCatalogo(CatCatalogosDB catCatalogosDB);

    public CatCatalogosDB  getCatCatalogo(int idCatalogo) throws ResourceNotFoundException;

    public void deleteCatCatalogo(int idCatalogo) throws ResourceNotFoundException;
}
