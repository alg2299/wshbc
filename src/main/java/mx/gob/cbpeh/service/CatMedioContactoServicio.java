package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatMedioContacto;


public interface CatMedioContactoServicio {

    public List < CatMedioContacto > getCatMedioContactos();

    public void saveCatMedioContacto(CatMedioContacto medioContacto);

    public Optional<CatMedioContacto>  getCatMedioContacto(int idMedioContacto) throws ResourceNotFoundException;

    public void deleteCatMedioContacto(int idMedioContacto) throws ResourceNotFoundException;
}
