package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatIdioma;


public interface CatIdiomaServicio {

    public List < CatIdioma > getCatIdiomas();

    public void saveCatIdioma(CatIdioma idioma);

    public Optional<CatIdioma>  getCatIdioma(int idIdioma) throws ResourceNotFoundException;

    public void deleteCatIdioma(int idIdioma) throws ResourceNotFoundException;
}
