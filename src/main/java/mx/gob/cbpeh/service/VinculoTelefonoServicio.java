package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.VinculoTelefono;


public interface VinculoTelefonoServicio {

    public List < VinculoTelefono > getVinculoTelefonos();

    public void saveVinculoTelefono(VinculoTelefono vinculoTelefono);

    public Optional<VinculoTelefono>  getVinculoTelefono(int idVinculoTelefono) throws ResourceNotFoundException;

    public void deleteVinculoTelefono(int idVinculoTelefono) throws ResourceNotFoundException;
}
