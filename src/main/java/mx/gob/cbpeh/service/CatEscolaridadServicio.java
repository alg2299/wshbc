package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEscolaridad;


public interface CatEscolaridadServicio {

    public List < CatEscolaridad > getCatEscolaridad();

    public void saveCatEscolaridad(CatEscolaridad escolaridad);

    public Optional<CatEscolaridad>  getCatEscolaridad(int idEscolaridad) throws ResourceNotFoundException;

    public void deleteCatEscolaridad(int idEscolaridad) throws ResourceNotFoundException;
}
