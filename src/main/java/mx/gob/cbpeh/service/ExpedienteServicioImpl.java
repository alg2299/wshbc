package mx.gob.cbpeh.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.Expediente;
import mx.gob.cbpeh.repository.ExpedienteRepositorio;

@Service
public class ExpedienteServicioImpl implements ExpedienteServicio {
	
	@Autowired
	private ExpedienteRepositorio expedienteRepositorio;

	@Override
	@Transactional
	public List<Expediente> getExpedientes() {
		return expedienteRepositorio.findAll();
	}

	@Override
	@Transactional
	public Expediente saveExpediente(Expediente expediente) {
		return expedienteRepositorio.save(expediente);
	}

	@Override
	@Transactional
	public Expediente getExpediente(String idExpediente) throws ResourceNotFoundException {
		return expedienteRepositorio.findByIdExpediente(idExpediente);
	}
	
	@Override
	@Transactional
	public String getFolioExpediente(int tipoExpediente) throws ResourceNotFoundException {
		return expedienteRepositorio.generarFolioExpediente(tipoExpediente);
	}

	@Override
	@Transactional
	public void deleteExpediente(int idExpediente) throws ResourceNotFoundException {
		expedienteRepositorio.deleteById(idExpediente);
	}

	@Override
	public Expediente getExpedienteIdPersonaDesaparecida(int idPersonaDesaparecida) throws ResourceNotFoundException {
		return expedienteRepositorio.findByIdPersonaDesaparecida(idPersonaDesaparecida);
	}

	@Override
	public List<Integer> obtenerAniosExpedientes() {
		return expedienteRepositorio.obtenerAniosExpedientes();
	}

}
