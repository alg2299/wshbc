package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatusLocalizado;
import mx.gob.cbpeh.repository.CatEstatusLocalizadoRepositorio;

@Service
public class CatEstatusLocalizadoServicioImpl implements CatEstatusLocalizadoServicio {

	@Autowired
	private CatEstatusLocalizadoRepositorio estatusLocalizadoRepositorio;

	@Override
	@Transactional
	public List<CatEstatusLocalizado> getCatEstatusLocalizados() {
		return estatusLocalizadoRepositorio.findAll();
	}

	@Override
	@Transactional
	public void saveCatEstatusLocalizado(CatEstatusLocalizado estatusLocalizado) {
		estatusLocalizadoRepositorio.save(estatusLocalizado);
		
	}

	@Override
	@Transactional
	public Optional<CatEstatusLocalizado> getCatEstatusLocalizado(int idEstatusLocalizado)
			throws ResourceNotFoundException {
		return estatusLocalizadoRepositorio.findById(idEstatusLocalizado);
	}

	@Override
	@Transactional
	public void deleteCatEstatusLocalizado(int idEstatusLocalizado) throws ResourceNotFoundException {
		estatusLocalizadoRepositorio.deleteById(idEstatusLocalizado);
		
	}
	
	

}
