package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatFormaNariz;


public interface CatFormaNarizServicio {

    public List < CatFormaNariz > getCatFormaNariz();

    public void saveCatFormaNariz(CatFormaNariz formaNariz);

    public Optional<CatFormaNariz>  getCatFormaNariz(int idFormaNariz) throws ResourceNotFoundException;

    public void deleteCatFormaNariz(int idFormaNariz) throws ResourceNotFoundException;
}
