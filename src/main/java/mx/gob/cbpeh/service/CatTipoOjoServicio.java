package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTipoOjo;


public interface CatTipoOjoServicio {

    public List < CatTipoOjo > getCatTipoOjos();

    public void saveCatTipoOjo(CatTipoOjo tipoOjo);

    public Optional<CatTipoOjo>  getCatTipoOjo(int idTipoOjo) throws ResourceNotFoundException;

    public void deleteCatTipoOjo(int idTipoOjo) throws ResourceNotFoundException;
}
