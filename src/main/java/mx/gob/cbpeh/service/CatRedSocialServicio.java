package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatRedSocial;


public interface CatRedSocialServicio {

    public List < CatRedSocial > getCatRedSocial();

    public void saveCatRedSocial(CatRedSocial redSocial);

    public Optional<CatRedSocial>  getCatRedSocial(int idRedSocial) throws ResourceNotFoundException;

    public void deleteCatRedSocial(int idRedSocial) throws ResourceNotFoundException;
}
