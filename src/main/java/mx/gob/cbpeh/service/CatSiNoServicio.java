package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatSiNo;


public interface CatSiNoServicio {

    public List < CatSiNo > getCatSiNo();

    public void saveCatSiNo(CatSiNo siNo);

    public Optional<CatSiNo>  getCatSiNo(int idSiNo) throws ResourceNotFoundException;

    public void deleteCatSiNo(int idSiNo) throws ResourceNotFoundException;
}
