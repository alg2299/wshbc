package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatArea;
import mx.gob.cbpeh.model.CatEstatus;


public interface CatAreaServicio {

    public List < CatArea > getCatAreas();
    
    public List < CatArea > getCatAreasActivas(CatEstatus catEstatus);

    public void saveCatArea(CatArea area);

    public Optional<CatArea>  getCatArea(int idArea) throws ResourceNotFoundException;

    public void deleteCatArea(int idArea) throws ResourceNotFoundException;
}
