package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTipoUso;


public interface CatTipoUsoServicio {

    public List < CatTipoUso > getCatTipoUsos();

    public void saveCatTipoUso(CatTipoUso tipoUso);

    public Optional<CatTipoUso>  getCatTipoUso(int idTipoUso) throws ResourceNotFoundException;

    public void deleteCatTipoUso(int idTipoUso) throws ResourceNotFoundException;
}
