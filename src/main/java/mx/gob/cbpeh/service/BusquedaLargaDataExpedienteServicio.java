package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.BusquedaLargaDataExpediente;


public interface BusquedaLargaDataExpedienteServicio {

    public List < BusquedaLargaDataExpediente > getBusquedaLargaDataExpediente();

    public void saveBusquedaLargaDataExpediente(BusquedaLargaDataExpediente busquedaLargaDataExpediente);

    public Optional<BusquedaLargaDataExpediente>  getBusquedaLargaDataExpediente(int idBusquedaLargaDataExpediente) throws ResourceNotFoundException;

    public void deleteBusquedaLargaDataExpediente(int idBusquedaLargaDataExpediente) throws ResourceNotFoundException;
}
