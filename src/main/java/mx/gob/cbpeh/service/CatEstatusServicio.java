package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;


public interface CatEstatusServicio {

    public List < CatEstatus > getCatEstatus();

    public void saveCatEstatus(CatEstatus estatus);

    public Optional<CatEstatus>  getCatEstatus(int idEstatus) throws ResourceNotFoundException;

    public void deleteCatEstatus(int idEstatus) throws ResourceNotFoundException;
}
