package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.CatParentesco;


public interface CatParentescoServicio {

    public List < CatParentesco > getCatParentescos();
    
    public List < CatParentesco > getCatParentescosActivos(CatEstatus catEstatus);
    

    public void saveCatParentesco(CatParentesco parentesco);

    public Optional<CatParentesco>  getCatParentesco(int idParentesco) throws ResourceNotFoundException;

    public void deleteCatParentesco(int idParentesco) throws ResourceNotFoundException;
}
