package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.Busqueda;


public interface BusquedaServicio {

    public List < Busqueda > getBusquedas();

    public Busqueda saveBusqueda(Busqueda busqueda);

    public Optional<Busqueda>  getBusqueda(int idBusqueda) throws ResourceNotFoundException;

    public void deleteBusqueda(int idBusqueda) throws ResourceNotFoundException;
}
