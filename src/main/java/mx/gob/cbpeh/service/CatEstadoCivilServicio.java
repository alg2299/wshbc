package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstadoCivil;


public interface CatEstadoCivilServicio {

    public List < CatEstadoCivil > getCatEstadoCivil();

    public void saveCatEstadoCivil(CatEstadoCivil estadoCivil);

    public Optional<CatEstadoCivil>  getCatEstadoCivil(int idEstadoCivil) throws ResourceNotFoundException;

    public void deleteCatEstadoCivil(int idEstadoCivil) throws ResourceNotFoundException;
}
