package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.BusquedaLargaData;


public interface BusquedaLargaDataServicio {

    public List < BusquedaLargaData > getBusquedaLargaDatas();

    public void saveBusquedaLargaData(BusquedaLargaData busquedaLargaData);

    public Optional<BusquedaLargaData>  getBusquedaLargaData(int idBusquedaLargaData) throws ResourceNotFoundException;

    public void deleteBusquedaLargaData(int idBusquedaLargaData) throws ResourceNotFoundException;
}
