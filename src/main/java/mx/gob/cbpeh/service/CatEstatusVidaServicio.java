package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatusVida;


public interface CatEstatusVidaServicio {

    public List < CatEstatusVida > getCatEstatusVida();

    public void saveCatEstatusVida(CatEstatusVida estatusVida);

    public Optional<CatEstatusVida>  getCatEstatusVida(int idEstatusVida) throws ResourceNotFoundException;

    public void deleteCatEstatusVida(int idEstatusVida) throws ResourceNotFoundException;
}
