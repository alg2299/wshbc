package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatBaseNariz;


public interface CatBaseNarizServicio {

    public List < CatBaseNariz > getCatBaseNariz();

    public void saveCatBaseNariz(CatBaseNariz baseNariz);

    public Optional<CatBaseNariz>  getCatBaseNariz(int idBaseNariz) throws ResourceNotFoundException;

    public void deleteCatBaseNariz(int idBaseNariz) throws ResourceNotFoundException;
}
