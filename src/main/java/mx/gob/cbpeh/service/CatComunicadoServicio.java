package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTipoComunicado;


public interface CatComunicadoServicio {

    public List < CatTipoComunicado > getCatTipoComunicados();

    public void saveCatTipoComunicado(CatTipoComunicado catTipoComunicado);

    public Optional<CatTipoComunicado>  getCatTipoComunicado(int idTipoComunicado) throws ResourceNotFoundException;

    public void deleteCatTipoComunicado(int idTipoComunicado) throws ResourceNotFoundException;
}
