package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.Bitacora;


public interface BitacoraServicio {

    public List < Bitacora > getBitacoras();

    public void saveBitacora(Bitacora bitacora);

    public Optional<Bitacora>  getBitacora(int idBitacora) throws ResourceNotFoundException;

    public void deleteBitacora(int idBitacora) throws ResourceNotFoundException;
}
