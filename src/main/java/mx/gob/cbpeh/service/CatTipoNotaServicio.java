package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.CatTipoNota;


public interface CatTipoNotaServicio {

    public List < CatTipoNota > getCatTipoNotas();
    
    public List < CatTipoNota > getCatTipoNotasActivas(CatEstatus catEstatus);

    public void saveCatTipoNota(CatTipoNota tipoNota);

    public Optional<CatTipoNota>  getCatTipoNota(int idTipoNota) throws ResourceNotFoundException;

    public void deleteCatTipoNota(int idTipoNota) throws ResourceNotFoundException;
}
