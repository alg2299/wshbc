package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.ArchivoLargaData;

public interface ArchivoLargaDataServicio {

    public List < ArchivoLargaData > getCArchivoLargaData();

    public void saveArchivoLargaData(ArchivoLargaData archivo);

    public Optional<ArchivoLargaData>  getArchivoLargaData(int idArchivoLargaData) throws ResourceNotFoundException;

    public void deleteArchivoLargaData(int idArchivoLargaData) throws ResourceNotFoundException;
}
