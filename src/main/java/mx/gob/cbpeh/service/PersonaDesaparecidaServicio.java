package mx.gob.cbpeh.service;

import java.util.List;
import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.PersonaDesaparecida;


public interface PersonaDesaparecidaServicio {

    public List < PersonaDesaparecida > getPersonaDesaparecidas();

    public PersonaDesaparecida savePersonaDesaparecida(PersonaDesaparecida personaDesaparecida);

    public PersonaDesaparecida getPersonaDesaparecida(int idPersonaDesaparecida) throws ResourceNotFoundException;
    
    //public Optional<PersonaDesaparecida>  getPersonaDesaparecida(int idPersonaDesaparecida) throws ResourceNotFoundException;

    public void deletePersonaDesaparecida(int idPersonaDesaparecida) throws ResourceNotFoundException;
    
    public PersonaDesaparecida  getPersonaDesaparecidaCurp(String curp) throws ResourceNotFoundException;
    
    public PersonaDesaparecida  getPersonaDesaparecidaConsultaExterna(String nombre,String aPaterno,String aMaterno,String curp) throws ResourceNotFoundException;
    
}
