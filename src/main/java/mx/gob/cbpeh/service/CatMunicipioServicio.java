package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatMunicipio;


public interface CatMunicipioServicio {

    public List < CatMunicipio > getCatMunicipios();
    
    public List<CatMunicipio> getCatMunicipiosPorEstado(String codigoEstado);
    
    public CatMunicipio getCatMunicipiosPorEstadoPorMunicipio(String codigoEstado, String codigoMunicipio);

    public void saveCatMunicipio(CatMunicipio municipio);

    public Optional<CatMunicipio>  getCatMunicipio(int idMunicipio) throws ResourceNotFoundException;

    public void deleteCatMunicipio(int idMunicipio) throws ResourceNotFoundException;
}
