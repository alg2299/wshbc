package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatPeriodicidad;


public interface CatPeriodicidadServicio {

    public List < CatPeriodicidad > getCatPeriodicidad();

    public void saveCatPeriodicidad(CatPeriodicidad periodicidad);

    public Optional<CatPeriodicidad>  getCatPeriodicidad(int idPeriodicidad) throws ResourceNotFoundException;

    public void deleteCatPeriodicidad(int idPeriodicidad) throws ResourceNotFoundException;
}
