package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatGenero;


public interface CatGeneroServicio {

    public List < CatGenero > getCatGenero();

    public void saveCatGenero(CatGenero genero);

    public Optional<CatGenero>  getCatGenero(int idGenero) throws ResourceNotFoundException;

    public void deleteCatGenero(int idGenero) throws ResourceNotFoundException;
}
