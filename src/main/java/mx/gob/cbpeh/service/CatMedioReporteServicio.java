package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatMedioReporte;


public interface CatMedioReporteServicio {

    public List < CatMedioReporte > getCatMedioReportes();

    public void saveCatMedioReporte(CatMedioReporte medioReporte);

    public Optional<CatMedioReporte>  getCatMedioReporte(int idMedioReporte) throws ResourceNotFoundException;

    public void deleteCatMedioReporte(int idMedioReporte) throws ResourceNotFoundException;
}
