package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatSexo;


public interface CatSexoServicio {

    public List < CatSexo > getCatSexo();

    public void saveCatSexo(CatSexo sexo);

    public Optional<CatSexo>  getCatSexo(int idSexo) throws ResourceNotFoundException;

    public void deleteCatSexo(int idSexo) throws ResourceNotFoundException;
}
