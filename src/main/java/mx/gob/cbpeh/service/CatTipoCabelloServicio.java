package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTipoCabello;


public interface CatTipoCabelloServicio {

    public List < CatTipoCabello > getCatTipoCabello();

    public void saveCatTipoCabello(CatTipoCabello tipoCabello);

    public Optional<CatTipoCabello>  getCatTipoCabello(int idTipoCabello) throws ResourceNotFoundException;

    public void deleteCatTipoCabello(int idTipoCabello) throws ResourceNotFoundException;
}
