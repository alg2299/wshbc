package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatAccion;


public interface CatAccionServicio {

    public List < CatAccion > getCatAcciones();

    public void saveCatAccion(CatAccion accion);

    public Optional<CatAccion>  getCatAccion(int idAccion) throws ResourceNotFoundException;

    public void deleteCatAccion(int idAccion) throws ResourceNotFoundException;
}
