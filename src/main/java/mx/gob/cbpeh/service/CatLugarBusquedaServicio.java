package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;
import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatLugarBusqueda;


public interface CatLugarBusquedaServicio {

    public List < CatLugarBusqueda > getCatLugarBusqueda();

    public CatLugarBusqueda saveCatLugarBusqueda(CatLugarBusqueda catLugarBusqueda);

    public Optional<CatLugarBusqueda>  getCatLugarBusqueda(int idCatLugarBusqueda) throws ResourceNotFoundException;

    public void deleteCatLugarBusqueda(int idCatLugarBusqueda) throws ResourceNotFoundException;
}
