package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatCompaniaTelefono;
import mx.gob.cbpeh.model.CatEstatus;


public interface CatCompaniaTelefonoServicio {

    public List < CatCompaniaTelefono > getCatCompaniaTelefonos();
    
    public List < CatCompaniaTelefono > getCatCompaniaTelefonosActivas(CatEstatus catEstatus);

    public void saveCatCompaniaTelefono(CatCompaniaTelefono companiaTelefono);

    public Optional<CatCompaniaTelefono>  getCatCompaniaTelefono(int idCompaniaTelefono) throws ResourceNotFoundException;

    public void deleteCatCompaniaTelefono(int idCompaniaTelefono) throws ResourceNotFoundException;
}
