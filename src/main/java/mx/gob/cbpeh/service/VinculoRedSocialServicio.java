package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.VinculoRedSocial;


public interface VinculoRedSocialServicio {

    public List < VinculoRedSocial > getVinculoRedSocial();

    public void saveVinculoRedSocial(VinculoRedSocial vinculoRedSocial);

    public Optional<VinculoRedSocial>  getVinculoRedSocial(int idVinculoRedSocial) throws ResourceNotFoundException;

    public void deleteVinculoRedSocial(int idVinculoRedSocial) throws ResourceNotFoundException;
}
