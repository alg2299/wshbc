package mx.gob.cbpeh.service;

import java.util.List;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.CatTipoComunicado;
import mx.gob.cbpeh.model.Comunicado;


public interface ComunicadoServicio {

    public List < Comunicado > getComunicados();
    
    public List < Comunicado > getComunicados(CatTipoComunicado catTipoComunicado);
   
    public void saveComunicado(Comunicado comunicado);

    public Comunicado getComunicado(int idComunicado) throws ResourceNotFoundException;

    public void deleteComunicado(int idComunicado) throws ResourceNotFoundException;
}
