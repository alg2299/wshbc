package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatFrente;


public interface CatFrenteServicio {

    public List < CatFrente > getCatFrente();

    public void saveCatFrente(CatFrente frente);

    public Optional<CatFrente>  getCatFrente(int idFrente) throws ResourceNotFoundException;

    public void deleteCatFrente(int idFrente) throws ResourceNotFoundException;
}
