package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatComplexion;


public interface CatComplexionServicio {

    public List < CatComplexion > getCatComplexiones();

    public void saveCatComplexion(CatComplexion complexion);

    public Optional<CatComplexion>  getCatComplexion(int idComplexion) throws ResourceNotFoundException;

    public void deleteCatComplexion(int idComplexion) throws ResourceNotFoundException;
}
