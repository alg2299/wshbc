package mx.gob.cbpeh.service;

import java.util.List;


import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.Expediente;


public interface ExpedienteServicio {

    public List < Expediente > getExpedientes();

    public Expediente saveExpediente(Expediente expediente);

    public Expediente  getExpediente(String idExpediente) throws ResourceNotFoundException;
    
    public String getFolioExpediente(int tipoExpediente) throws ResourceNotFoundException;

    public void deleteExpediente(int idExpediente) throws ResourceNotFoundException;
    
    public Expediente  getExpedienteIdPersonaDesaparecida(int idPersonaDesaparecida) throws ResourceNotFoundException;
    
    public List<Integer> obtenerAniosExpedientes();
}
