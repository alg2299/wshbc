package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.CatPuesto;


public interface CatPuestoServicio {

    public List < CatPuesto > getCatPuestos();
    
    public List < CatPuesto > getCatPuestosActivos(CatEstatus catEstatus);

    public void saveCatPuesto(CatPuesto puesto);

    public Optional<CatPuesto>  getCatPuesto(int idPuesto) throws ResourceNotFoundException;

    public void deleteCatPuesto(int idPuesto) throws ResourceNotFoundException;
}
