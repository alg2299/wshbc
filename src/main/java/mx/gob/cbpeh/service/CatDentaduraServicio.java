package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatDentadura;


public interface CatDentaduraServicio {

    public List < CatDentadura > getCatDentaduras();

    public void saveCatDentadura(CatDentadura dentadura);

    public Optional<CatDentadura>  getCatDentadura(int idDentadura) throws ResourceNotFoundException;

    public void deleteCatDentadura(int idDentadura) throws ResourceNotFoundException;
}
