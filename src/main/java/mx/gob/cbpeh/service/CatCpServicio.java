package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatCp;


public interface CatCpServicio {

    public List < CatCp > getCatCps();
    
    public List<CatCp> getCatCpPorEstadoMunicipio(String codigoEstado,String codigoMunicipio);
    
    public CatCp getCpCodigoCP(String codigoCp);

    public void saveCatCps(CatCp cp);

    public Optional<CatCp>  getCatCp(int idCp) throws ResourceNotFoundException;

    public void deleteCatCp(int idCp) throws ResourceNotFoundException;
}
