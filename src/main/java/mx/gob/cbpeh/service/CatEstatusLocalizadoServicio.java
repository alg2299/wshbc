package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatEstatusLocalizado;


public interface CatEstatusLocalizadoServicio {

    public List < CatEstatusLocalizado > getCatEstatusLocalizados();

    public void saveCatEstatusLocalizado(CatEstatusLocalizado estatusLocalizado);

    public Optional<CatEstatusLocalizado>  getCatEstatusLocalizado(int idEstatusLocalizado) throws ResourceNotFoundException;

    public void deleteCatEstatusLocalizado(int idEstatusLocalizado) throws ResourceNotFoundException;
}
