package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatTipoIdentificacion;


public interface CatTipoIdentificacionServicio {

    public List < CatTipoIdentificacion > getCatTipoIdentificacion();

    public void saveCatTipoIdentificacion(CatTipoIdentificacion tipoIdentificacion);

    public Optional<CatTipoIdentificacion>  getCatTipoIdentificacion(int idTipoIdentificacion) throws ResourceNotFoundException;

    public void deleteCatTipoIdentificacion(int idTipoIdentificacion) throws ResourceNotFoundException;
}
