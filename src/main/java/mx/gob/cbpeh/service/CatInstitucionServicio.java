package mx.gob.cbpeh.service;

import java.util.List;
import java.util.Optional;

import mx.gob.cbpeh.exception.ResourceNotFoundException;
import mx.gob.cbpeh.model.CatInstitucion;


public interface CatInstitucionServicio {

    public List < CatInstitucion > getCatInstitucion();

    public void saveCatInstitucion(CatInstitucion estado);

    public Optional<CatInstitucion>  getCatInstitucion(int idInstitucion) throws ResourceNotFoundException;

    public void deleteCatInstitucion(int idInstitucion) throws ResourceNotFoundException;
}
