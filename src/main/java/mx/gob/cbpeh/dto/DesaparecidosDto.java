package mx.gob.cbpeh.dto;


public class DesaparecidosDto {
	
	private String id_expediente;	
	private String id_edad;
	private String id_sexo;
	private String id_estado;
	private String id_municipio;
	private String id_cp;
	private String fecha_desaparicion;
	private String id_estatus_localizado;
	
	public String getId_expediente() {
		return id_expediente;
	}
	public void setId_expediente(String id_expediente) {
		this.id_expediente = id_expediente;
	}
	public String getId_edad() {
		return id_edad;
	}
	public void setId_edad(String id_edad) {
		this.id_edad = id_edad;
	}
	public String getId_sexo() {
		return id_sexo;
	}
	public void setId_sexo(String id_sexo) {
		this.id_sexo = id_sexo;
	}
	public String getId_estado() {
		return id_estado;
	}
	public void setId_estado(String id_estado) {
		this.id_estado = id_estado;
	}
	public String getId_municipio() {
		return id_municipio;
	}
	public void setId_municipio(String id_municipio) {
		this.id_municipio = id_municipio;
	}
	public String getId_cp() {
		return id_cp;
	}
	public void setId_cp(String id_cp) {
		this.id_cp = id_cp;
	}
	public String getFecha_desaparicion() {
		return fecha_desaparicion;
	}
	public void setFecha_desaparicion(String fecha_desaparicion) {
		this.fecha_desaparicion = fecha_desaparicion;
	}
	public String getId_estatus_localizado() {
		return id_estatus_localizado;
	}
	public void setId_estatus_localizado(String id_estatus_localizado) {
		this.id_estatus_localizado = id_estatus_localizado;
	}
	
	
		
}
