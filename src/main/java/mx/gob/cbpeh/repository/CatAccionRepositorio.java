package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.CatAccion;

@Repository("accionRepositorio")
public interface CatAccionRepositorio extends JpaRepository<CatAccion, Integer> {

}
