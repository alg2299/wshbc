package mx.gob.cbpeh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("perfilRepositorio")
public interface PerfilRepositorio extends JpaRepository<Perfil, Integer>{
	
	List<Perfil> findByCatEstatus(CatEstatus catEstatus);

}
