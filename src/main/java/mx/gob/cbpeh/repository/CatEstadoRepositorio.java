package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("estadoRepositorio")
public interface CatEstadoRepositorio extends JpaRepository<CatEstado, Integer>{
	
	public CatEstado findByCodigoEstado(String codigoEstado);

}
