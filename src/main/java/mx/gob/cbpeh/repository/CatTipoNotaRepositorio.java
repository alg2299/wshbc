package mx.gob.cbpeh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("tipoNotaRepositorio")
public interface CatTipoNotaRepositorio extends JpaRepository<CatTipoNota, Integer>{
	
	List<CatTipoNota> findByCatEstatus(CatEstatus catEstatus);

}
