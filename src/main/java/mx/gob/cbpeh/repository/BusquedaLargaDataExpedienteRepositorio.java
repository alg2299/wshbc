package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.BusquedaLargaDataExpediente;

    @Repository("busquedaLargaDataExpedienteRepositorio")
public interface BusquedaLargaDataExpedienteRepositorio extends JpaRepository<BusquedaLargaDataExpediente, Integer>{

}
