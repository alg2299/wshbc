package mx.gob.cbpeh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model  .*;

@Repository("coloniaRepositorio")
public interface CatColoniaRepositorio extends JpaRepository<CatColonia, Integer>{
	
	List<CatColonia> findByCodigoCp(String codigoCp);

}
