package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("personaDesaparecidaRepositorio")
public interface PersonaDesaparecidaRepositorio extends JpaRepository<PersonaDesaparecida, Integer>{
	
PersonaDesaparecida findByIdPersonaDesaparecida(int id);

PersonaDesaparecida findByCurp(String curp);

PersonaDesaparecida findByNombreLikeAndApaternoLikeAndAmaternoLikeAndCurpLike(String nombre,String aPaterno,String aMaterno, String curp);


}
