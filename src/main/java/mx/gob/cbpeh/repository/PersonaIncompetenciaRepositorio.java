package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("personaIncompetenciaRepositorio")
public interface PersonaIncompetenciaRepositorio extends JpaRepository<PersonaIncompetencia, Integer>{

}
