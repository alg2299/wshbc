package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("personaEntrevistaRepositorio")
public interface PersonaEntrevistaRepositorio extends JpaRepository<PersonaEntrevista, Integer>{
	
	public PersonaEntrevista findByIdExpedienteAndIdEstatus(String idExpediente, Integer idEstatus);

}
