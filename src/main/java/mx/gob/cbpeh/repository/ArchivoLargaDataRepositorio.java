package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.ArchivoLargaData;

@Repository("archivoLargaDataRepositorio")
public interface ArchivoLargaDataRepositorio extends JpaRepository<ArchivoLargaData, Integer> {

}
