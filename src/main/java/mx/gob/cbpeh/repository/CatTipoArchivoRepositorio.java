package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("tipoArchivoRepositorio")
public interface CatTipoArchivoRepositorio extends JpaRepository<CatTipoArchivo, Integer>{

	CatTipoArchivo findByTipoArchivoDetalle(String extension);
}
