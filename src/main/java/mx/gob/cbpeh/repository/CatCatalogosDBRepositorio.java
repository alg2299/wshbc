package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("catCatalogosDBRepositorio")
public interface CatCatalogosDBRepositorio extends JpaRepository<CatCatalogosDB, Integer>{
	
	CatCatalogosDB findByIdCatalogo(int idCatalogo);

}
