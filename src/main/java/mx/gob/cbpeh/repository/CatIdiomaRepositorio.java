package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("idiomaRepositorio")
public interface CatIdiomaRepositorio extends JpaRepository<CatIdioma, Integer>{

}
