package mx.gob.cbpeh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("generoRepositorio")
public interface CatGeneroRepositorio extends JpaRepository<CatGenero, Integer>{

}
