package mx.gob.cbpeh.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.gob.cbpeh.model.*;

@Repository("municipioRepositorio")
public interface CatMunicipioRepositorio extends JpaRepository<CatMunicipio, Integer>{
	
	List<CatMunicipio> findByCodigoEstado(String codigoEstado);
	
	CatMunicipio findByCodigoEstadoAndCodigoMunicipio(String codigoEstado, String codigoMunicipio);
	
}
