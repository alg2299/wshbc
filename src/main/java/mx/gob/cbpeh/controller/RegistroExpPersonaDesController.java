package mx.gob.cbpeh.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.json.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import mx.gob.cbpeh.dto.DesaparecidosDto;
import mx.gob.cbpeh.model.CatCp;
import mx.gob.cbpeh.model.CatEdad;
import mx.gob.cbpeh.model.CatEstado;
import mx.gob.cbpeh.model.CatEstatus;
import mx.gob.cbpeh.model.CatEstatusLocalizado;
import mx.gob.cbpeh.model.CatMunicipio;
import mx.gob.cbpeh.model.CatSexo;
import mx.gob.cbpeh.model.Expediente;
import mx.gob.cbpeh.model.PersonaDesaparecida;
import mx.gob.cbpeh.service.CatCpServicio;
import mx.gob.cbpeh.service.CatEstadoServicio;
import mx.gob.cbpeh.service.CatMunicipioServicio;
import mx.gob.cbpeh.service.ExpedienteServicio;
import mx.gob.cbpeh.service.PersonaDesaparecidaServicio;
import mx.gob.cbpeh.dto.ExpedienteRequest;
import mx.gob.cbpeh.dto.ExpedienteResponse;
import mx.gob.cbpeh.exception.ResourceNotFoundException;


@RestController
@RequestMapping(value = "/api")
public class RegistroExpPersonaDesController {

	@Autowired
	private PersonaDesaparecidaServicio personaDesaparecida;
	@Autowired
	ExpedienteServicio expedienteServicios;
	@Autowired
	CatMunicipioServicio catMunicipioServicio;
	@Autowired
	CatCpServicio catCpServicio;
	@Autowired
	CatEstadoServicio catEstadoServicio;

	@RequestMapping(value = "/infoExpediente", method = RequestMethod.GET, produces = "application/json")
	private ResponseEntity<String> guardarExpPersonaDesaparecida() {

		String response = "check";

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/guardarDesaparecido", method = RequestMethod.POST, consumes = "application/json", produces = "application/json; Charset=UTF-8")
	public ResponseEntity<String> guardarDesaparecido(@RequestBody DesaparecidosDto desaparecido)
			throws IOException, ResourceNotFoundException {

		JSONObject response = new JSONObject();
		response.put("estatus", "Transacción realizada con éxito");

		if (desaparecido.getId_expediente().equals("") || desaparecido.getId_sexo().equals("")
				|| desaparecido.getId_edad().equals("") || desaparecido.getId_estado().equals("")
				|| desaparecido.getId_municipio().equals("") || desaparecido.getId_cp().equals("")
				|| desaparecido.getFecha_desaparicion().equals("")
				|| desaparecido.getId_estatus_localizado().equals("")) {

			response.put("estatus", "Error: faltan campos obligatorios");

		} else {
			if (expedienteServicios.getExpediente(desaparecido.getId_expediente()) != null) {

				response.put("estatus", "Error: número de expediente ya existe en el sistema.");

			} else {

				PersonaDesaparecida personaDesaparece = new PersonaDesaparecida();

				CatMunicipio municipio = new CatMunicipio();
				municipio = getIdMunicipioPorCEstadoCMunicipio(desaparecido.getId_estado(),
						desaparecido.getId_municipio());

				CatCp catCp = new CatCp();
				catCp = getCpCodigoCP(desaparecido.getId_cp());
				
				CatEstado catEstado = new CatEstado();
				catEstado = getEstadoCodigoEstado(desaparecido.getId_estado());

				if (municipio.getCodigoMunicipio() != null && catCp.getCodigoCp() != null && catEstado.getCodigoEstado() != null) {

					CatEdad catEdad = new CatEdad();
					if (!desaparecido.getId_edad().equals("")) {
						catEdad.setIdEdad(Integer.parseInt((desaparecido.getId_edad()))+ 1);
						personaDesaparece.setCatEdad(catEdad);
					}
					
					personaDesaparece.setCatEstado(catEstado);					

					CatSexo catSexo = new CatSexo();
					if (!desaparecido.getId_sexo().equals("")) {
						catSexo.setIdSexo(Integer.parseInt(desaparecido.getId_sexo()));
						personaDesaparece.setCatSexo(catSexo);
					}

					personaDesaparece.setCatMunicipio(municipio);
					personaDesaparece.setCatCp(catCp);

					LocalDate dates = LocalDate.now();
					CatEstatus estatus = new CatEstatus();
					estatus.setIdEstatus(1);
					personaDesaparece.setCatEstatus(estatus);
					if (!desaparecido.getFecha_desaparicion().equals("")) {
						personaDesaparece.setFechaDesaparcion(Date.valueOf(desaparecido.getFecha_desaparicion()));
					}
					personaDesaparece.setIdUsuarioAlta("cesar_perez");
					personaDesaparece.setFechaAlta(Date.valueOf(dates));

					PersonaDesaparecida personaDesa =personaDesaparecida.savePersonaDesaparecida(personaDesaparece);

					Expediente expedienteCompetente = new Expediente();

					expedienteCompetente.setIdUsuarioAlta("cesar_perez");
					expedienteCompetente.setFechaAlta(Date.valueOf(dates));
					expedienteCompetente.setFechaAperturaExpediente(Date.valueOf(dates));
					// expedienteCompetente.setHrAperturaExpediente(hrAperturaExpediente);
					expedienteCompetente.setIdExpediente(desaparecido.getId_expediente().toUpperCase());
					CatEstatusLocalizado catEstatusLocalizado = new CatEstatusLocalizado();
					catEstatusLocalizado.setIdEstatusLocalizado(Integer.parseInt(desaparecido.getId_estatus_localizado()));
					expedienteCompetente.setCatEstatusLocalizado(catEstatusLocalizado);
					expedienteCompetente.setCatEstatus(estatus);
					expedienteCompetente.setIdPersonaDesaparecida(personaDesa.getIdPersonaDesaparecida());
					expedienteServicios.saveExpediente(expedienteCompetente);
				} else {
					response.put("estatus", "Error: código recibido no existe en el catálogo.");
				}
			}
		}

		return new ResponseEntity<>(response.toString(), HttpStatus.OK);
	}

	/* Obtiene todos los cp del municipio que se envia */
	// @RequestMapping(value = "/municipios-por-estado", method =
	// RequestMethod.POST, produces = "application/json")//@ResponseBody

	public CatMunicipio getIdMunicipioPorCEstadoCMunicipio(String codigoEstado, String codigoMunicipio) {
		CatMunicipio catMunicipio = new CatMunicipio();
		if (catMunicipioServicio.getCatMunicipiosPorEstadoPorMunicipio(codigoEstado, codigoMunicipio) != null) {
			catMunicipio = (catMunicipioServicio.getCatMunicipiosPorEstadoPorMunicipio(codigoEstado, codigoMunicipio));
		}
		return catMunicipio;
	}

	// @RequestMapping(value = "/cp-por-estado-municipios", method =
	// RequestMethod.POST, produces = "application/json")//@ResponseBody
	public CatCp getCpCodigoCP(String codigoCP) {
		CatCp cp = new CatCp();
		if (catCpServicio.getCpCodigoCP(codigoCP) != null) {
			cp = catCpServicio.getCpCodigoCP(codigoCP);
		}
		return cp;
	}
	
	public CatEstado getEstadoCodigoEstado(String codigoEstado) {
		CatEstado estado = new CatEstado();
		if (catEstadoServicio.getEstadoCodigoEstado(codigoEstado) != null) {
			estado = catEstadoServicio.getEstadoCodigoEstado(codigoEstado);
		}
		return estado;
	}
}
